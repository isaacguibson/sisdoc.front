export const environment = {
  production: true,
  apiUrl: 'https://ec2-3-132-96-14.us-east-2.compute.amazonaws.com:8082/sisdoc-0.0.1-SNAPSHOT/',
  appName: 'Sistema de Gerenciamento de Documentos',
  appAlias: 'sisdoc',
  appUUID: 'ea393197-8ef3-4298-82b6-b03a46080518'
};
