export class Reuniao{
    id?: number;
    colegiadoId: number;
    tipo: string;
    numero: number;
    hora: string;
}