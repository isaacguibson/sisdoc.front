export class Colegiado{
    id?: number;
    nome: string;
    descricao: string;
    setorId: number;
    membrosIds: [];
}