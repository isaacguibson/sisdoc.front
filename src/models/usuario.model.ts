export class Usuario{
    id?: number;
    nome: string;
    email: string;
    senha: string;
    setorId?: number;
    cargoId?: number;
    tratamento: string;
    matricula: string;
    nomeSetor: string;
    nomeCargo: string;
    curso: string;
}