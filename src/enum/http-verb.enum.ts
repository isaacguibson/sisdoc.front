export enum HttpVerb {
    GET, POST, PUT, DELETE
}