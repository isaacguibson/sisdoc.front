# Sisdoc

O projeto foi gerado com [Angular CLI](https://github.com/angular/angular-cli) versão 7.1.3.

## Development server

Rode `npm install` para instalar as dependências em seguida rode `ng serve` para um deploy em desenvolvimento. Dpois disso vá para `http://localhost:4200/`.

## Build

Rode `ng build` para construir o projeto. Os arquivos contruídos ficarão no diretório `dist/`. Use a `--prod` flag para construção em produção.
